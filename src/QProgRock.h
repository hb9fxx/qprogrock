#pragma once

#include "SerialReader.h"
#include "Config.h"

#include <QObject>
#include <QSerialPortInfo>
#include <QString>
#include <QStringList>
#include <QThread>

#include <atomic>
#include <iostream>
#include <memory>

class QProgRock : public QThread {
    Q_OBJECT

    Q_PROPERTY(QString programName READ programName CONSTANT)
    Q_PROPERTY(QString programVersion READ programVersion CONSTANT)
    Q_PROPERTY(QString qtVersion READ qtVersion CONSTANT)

    Q_PROPERTY(QStringList deviceList READ deviceList NOTIFY deviceListChanged)

    Q_PROPERTY(QString deviceName READ deviceName WRITE setDeviceName NOTIFY connectionChanged)
    Q_PROPERTY(bool connected READ connected NOTIFY connectionChanged)
    Q_PROPERTY(QString firmware READ firmwareVersionString NOTIFY connectionChanged)

    Q_PROPERTY(int eepromId READ getEepromId WRITE setEepromId NOTIFY changed)
    Q_PROPERTY(int refClk READ getRefClk WRITE setRefClk NOTIFY changed)
    Q_PROPERTY(int gpsCorrectionThreshold READ getGpsCorrectionThreshold WRITE setGpsCorrectionThreshold NOTIFY changed)
    Q_PROPERTY(int calibration READ getCalibration NOTIFY changed)

    Q_PROPERTY(int bank READ getBank WRITE setBank NOTIFY changed)
    Q_PROPERTY(int clk0 READ getClk0 WRITE setClk0 NOTIFY changed)
    Q_PROPERTY(int clk1 READ getClk1 WRITE setClk1 NOTIFY changed)
    Q_PROPERTY(int clk2 READ getClk2 WRITE setClk2 NOTIFY changed)

public:
    QProgRock();
    ~QProgRock() override;

    QString programName() { return QPROGROCK_NAME; };
    QString programVersion() { return "v" + QString::number(QPROGROCK_VERSION_MAJOR) + "." + QString::number(QPROGROCK_VERSION_MINOR) + "." + QString::number(QPROGROCK_VERSION_PATCH); }
    QString qtVersion() { return QString("Qt v") + qVersion(); };

    QStringList deviceList();

    QString deviceName() { return _deviceName; };
    bool connected() { return _connected; };
    QString firmwareVersionString() { return _firmwareVersionString; };

    int getEepromId() { if (!_reader) { return 0; } return _register_map.at(1); };
    int getRefClk() { if (!_reader) { return 0; } return _register_map.at(2); };
    int getGpsCorrectionThreshold() { if (!_reader) { return 0; } return _register_map.at(3); };
    int getCalibration() { if (!_reader) { return 0; } return _register_map.at(28); };

    int getBank() { return _bank; };
    int getClk0() { if (!_reader) { return 0; } return _register_map.at(4 + 3 * _bank); };
    int getClk1() { if (!_reader) { return 0; } return _register_map.at(5 + 3 * _bank); };
    int getClk2() { if (!_reader) { return 0; } return _register_map.at(6 + 3 * _bank); };

signals:
    void deviceListChanged();
    void connectionChanged();
    void changed();

public slots:
    void listSerialPorts();

    void setDeviceName(QString const & deviceName);

    void setEepromId(int eepromId) { if (_reader) { _register_map.at(1) = _reader->setRegister(1, eepromId); } emit changed(); };
    void setRefClk(int refClk) { if (_reader) { _register_map.at(2) = _reader->setRegister(2, refClk); } emit changed(); };
    void setGpsCorrectionThreshold(int gpsCorrectionThreshold) { if (_reader) { _register_map.at(3) = _reader->setRegister(3, gpsCorrectionThreshold); } emit changed(); };

    void setBank(int bank) { _bank = bank; emit changed(); };
    void setClk0(int clk0) { if (_reader) { _register_map.at(4 + 3 * _bank) = _reader->setRegister(4 + 3 * _bank, clk0); } emit changed(); };
    void setClk1(int clk1) { if (_reader) { _register_map.at(5 + 3 * _bank) = _reader->setRegister(5 + 3 * _bank, clk1); } emit changed(); };
    void setClk2(int clk2) { if (_reader) { _register_map.at(6 + 3 * _bank) = _reader->setRegister(6 + 3 * _bank, clk2); } emit changed(); };

private:
    void run() override;
    void stop();
    void probeFirmware();

    enum class FirmwareVersion { PR1012, PR103 };
    FirmwareVersion _firmwareVersion{FirmwareVersion::PR1012};
    QString _firmwareVersionString;

    QString _deviceName;
    QList<QSerialPortInfo> _serialPorts;

    std::unique_ptr<SerialReader> _reader;
    bool _connected{false};

    int _bank{0};
    std::map<int, int> _register_map;

    std::atomic_bool _run;
};
