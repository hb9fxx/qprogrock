#include <QGuiApplication>
#include <QtQml>
#include <QQmlApplicationEngine>

#include "QProgRock.h"

int main(int argc, char *argv[]) {
    QGuiApplication app(argc, argv);

    qmlRegisterType<QProgRock>("qprogrock", 0, 1, "QProgRock");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
