#include "QProgRock.h"

#include <algorithm>
#include <cstring>
#include <chrono>
#include <thread>

QProgRock::QProgRock() :
QThread(nullptr),
_run{false} {
}

QProgRock::~QProgRock() {
    if (_run) {
        stop();
        wait();
    }
}

QStringList QProgRock::deviceList() {
    QStringList list;

    std::transform(
        std::begin(_serialPorts),
        std::end(_serialPorts),
        std::back_inserter(list),
        [](auto const & e) { return e.portName(); }
    );

    std::sort(std::begin(list), std::end(list));

    return list;
}

void QProgRock::listSerialPorts() {
    _serialPorts.clear();

    auto serialPorts = QSerialPortInfo::availablePorts();

    for (auto const & port: serialPorts) {
        _serialPorts.append(port);
    }

    emit deviceListChanged();
}

void QProgRock::setDeviceName(QString const & deviceName) {
    if (_run) {
        stop();
        wait();
    }

    _deviceName = deviceName;

    _run = true;
    start();
}

void QProgRock::run() {
    _connected = false;
    emit connectionChanged();

    auto d = std::find_if(
            std::begin(_serialPorts),
            std::end(_serialPorts),
            [this](auto const & e){ return e.portName() == _deviceName; }
    );

    if (d == std::end(_serialPorts)) {
        _deviceName = "";

        quit();
        return;
    }

    try {
        _reader = std::make_unique<SerialReader>(d->systemLocation());
        probeFirmware();
        _register_map = _reader->readRegisters();
    } catch (std::exception const & e) {
        _reader.reset();
        _deviceName = "";

        quit();
        return;
    }

    _connected = true;

    emit connectionChanged();

    emit changed();

    // RefClk loop
    while (_run) {
        try {
            auto r2 = _register_map.at(2);
            auto r28 = _register_map.at(28);
            _register_map.at(2) = _reader->readRegister(2);
            _register_map.at(28) = _reader->readRegister(28);
            if ((_register_map.at(2) != r2) || (_register_map.at(28) != r28)) { emit changed(); }
        } catch (std::exception const & e) {
            _reader.reset();
            _deviceName = "";

            _connected = false;
            emit connectionChanged();

            quit();
            return;
        }
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    _reader.reset();
    _deviceName = "";
    quit();
}

void QProgRock::stop() {
    _run = false;
}

void QProgRock::probeFirmware() {
    try {
        // Probe PR1.03 by using the new command to write a temp value
        // First reading its value to be able to restore
        auto r27 = _reader->readRegister(27);
        _reader->setRegister(27, 10000, false);
        _reader->setRegister(27, r27, false);
        _firmwareVersion = FirmwareVersion::PR103;
        _firmwareVersionString = "pr1.03";
    } catch (std::exception const & e) {
        // If Unsupported command thrown, it's PR1.01, PR1.01a or PR1.02
        // No solution to identify them more precisely
        if (std::strncmp(e.what(), "Unsupported command", 20) == 0) {
            _firmwareVersion = FirmwareVersion::PR1012;
            _firmwareVersionString = "pr1.01(a)/02";
        } else {
            throw e;
        }
    }

    return;
}
