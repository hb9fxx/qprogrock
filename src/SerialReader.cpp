#include "SerialReader.h"

#include <algorithm>
#include <iostream>

SerialReader::SerialReader(QString const & serialPortName) :
_serialPortName{serialPortName} {
    if (_serialPortName.compare("") == 0) {
        throw std::runtime_error("Invalid serialPortName");
    }

    _serialPort.setPortName(_serialPortName);
    _serialPort.setBaudRate(QSerialPort::Baud9600);

    if (!_serialPort.open(QIODevice::ReadWrite)) {
        std::runtime_error(QObject::tr("Failed to open port, error: %1").arg(_serialPort.errorString()).toStdString());
    }
}

SerialReader::~SerialReader() {
    _serialPort.close();
}

std::map<int, int> SerialReader::readRegisters() {
    std::string response;
    std::map<int, int> register_map;

    for (auto const & c: _sendCommand("?\r")) {
        if (c == '\r') {
            register_map.insert(_parseResponse(response));
            response.clear();
        } else if (c == ',') {
            // ignore
        } else {
            response.append(1, c);
        }
    }

    return register_map;
}

int SerialReader::readRegister(int reg) {
    std::string command;
    std::string response;

    command.append(std::to_string(reg));
    command.append("?");
    command.append("\r");

    for (auto const & c: _sendCommand(command)) {
        if (c == '\r') {
            return _parseResponse(response).second;
        } else if (c == ',') {
            // ignore
        } else {
            response.append(1, c);
        }
    }

    throw std::runtime_error("Invalid response received");
}

int SerialReader::setRegister(int reg, int value, bool persistent) {
    std::string command;
    std::string response;

    command.append(std::to_string(reg));
    command.append(persistent ? "=" : ":");
    command.append(std::to_string(value));
    command.append("\r");

    auto reply = _sendCommand(command);

    if (reply.compare("Error: Cmd\r") == 0) {
      throw std::runtime_error("Unsupported command");
    }

    for (auto const & c: reply) {
        if (c == '\r') {
            try {
                return _parseResponse(response).second;
            } catch (std::exception const & e) {
              throw std::runtime_error("Invalid response received:\n" + reply);
            }
        } else if (c == ',') {
            // ignore
        } else {
            response.append(1, c);
        }
    }

    throw std::runtime_error("Invalid response received");
}

std::string SerialReader::_sendCommand(std::string const & command) {
    std::lock_guard<std::mutex> lock(_lock);

    if (_serialPort.write(QByteArray{command.c_str()}) != command.size()) {
        throw std::runtime_error("Couldn't send command " + command);
    }

    if (!_serialPort.waitForReadyRead(1000)) {
        throw std::runtime_error("Didn't get response to command " + command);
    }

    QByteArray readData;

    while (_serialPort.waitForReadyRead(10)) {
        readData.append(_serialPort.readAll());

        if (_serialPort.error() == QSerialPort::ReadError) {
            break;
        } else if (_serialPort.error() == QSerialPort::TimeoutError && readData.isEmpty()) {
            break;
        }
    }

    if (readData.isEmpty()) {
        throw std::runtime_error("Didn't get response to command " + command);
    }

    std::string response;

    std::copy(std::begin(readData), std::end(readData), std::back_inserter(response));

    return response;
}

std::pair<int, int> SerialReader::_parseResponse(std::string const & response) {
    auto i = std::find(std::begin(response), std::end(response), ':');

    if (i != std::end(response)) {
        std::string key, value;

        std::copy(std::begin(response), i, std::begin(key));
        std::copy(i + 1, std::end(response), std::begin(value));

        return std::make_pair(std::stoi(key), std::stoi(value));
    }

    throw std::runtime_error("Error parsing response " + response);
}
