#pragma once

#include <QObject>
#include <QSerialPort>
#include <QString>

#include <map>
#include <mutex>
#include <string>
#include <utility>

class SerialReader : public QObject {
    Q_OBJECT

public:
    explicit SerialReader(QString const & serialPortName = "");
    ~SerialReader() override;

    std::map<int, int> readRegisters();
    int readRegister(int reg);
    int setRegister(int reg, int value, bool persistent=true);

private:
    std::string _sendCommand(std::string const & command);
    static std::pair<int, int> _parseResponse(std::string const & response);

    QString _serialPortName;
    QSerialPort _serialPort;

    std::mutex _lock;
};
