import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import QtQuick.Extras 1.4
import QtQuick.Controls.Styles 1.0

import qprogrock 0.1

ApplicationWindow {
    id: root
    visible: true
    width: 640
    height: 520
    title: qprogrock.programName + " - " + qprogrock.programVersion + " by HB9FXX"

    QProgRock {
        id: qprogrock
    }

    StackView {
        id: stack
        initialItem: mainView
        anchors.fill: parent
    }

    Component {
        id: mainView

        ColumnLayout {
            RowLayout {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                Text {
                    font.pixelSize: 24
                    color: "#aaffffff"
                    text: "Bank: "
                }

                ComboBox {
                    model: ["0", "1", "2", "3", "4", "5", "6", "7"]
                    onActivated: qprogrock.bank = currentIndex
                    Component.onCompleted: currentIndex = qprogrock.bank
                    Layout.preferredWidth: 100

                    ToolTip.text: qsTr("Select the bank set on your QRP-Labs ProgRock")
                    ToolTip.visible: hovered
                }

                Item {
                    Layout.fillWidth: true
                }

                Text {
                    font.pixelSize: 24
                    color: "#aaffffff"
                    text: "Step: "
                }

                ComboBox {
                    id: step
                    model: [
                        qsTr("1Hz"),
                        qsTr("10Hz"),
                        qsTr("100Hz"),
                        qsTr("1kHz"),
                        qsTr("10kHz"),
                        qsTr("100kHz"),
                        qsTr("1MHz"),
                        qsTr("10MHz"),
                        qsTr("100MHz")
                    ]
                    Layout.preferredWidth: 150

                    ToolTip.text: qsTr("Frequency adjustement step")
                    ToolTip.visible: hovered
                }

                Text {
                    font.pixelSize: 24
                    color: "#aaffffff"
                    text: "Hz"
                }
            }

            RowLayout {
                Text {
                    font.pixelSize: 24
                    color: "#aaffffff"
                    text: "Clk0:"
                    Layout.preferredWidth: 100
                }

                RadioButton {
                    text: qsTr("Off")
                    checked: qprogrock.clk0 == 0 ? true : false
                    onClicked: qprogrock.clk0 = 0

                    ToolTip.text: qsTr("Turn off clock 0")
                    ToolTip.visible: hovered
                }

                RadioButton {
                    text: qsTr("On")
                    checked: qprogrock.clk0 >= 3500 ? true : false
                    onClicked: qprogrock.clk0 = clk0.value

                    ToolTip.text: qsTr("Turn on clock 0")
                    ToolTip.visible: hovered
                }
            }

            RowLayout {
                id: clk0row
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                SpinBox {
                    id: clk0
                    font.pixelSize: 48
                    value: qprogrock.clk0 < 3500 ? value : qprogrock.clk0
                    from: 3500
                    to: 200000000
                    stepSize: Math.pow(10, step.currentIndex)
                    editable: true
                    onValueModified: qprogrock.clk0 = clk0.value
                    Layout.preferredWidth: 450
                    enabled: qprogrock.clk0 == 0 ? false : true

                    ToolTip.text: qsTr("Set clock 0 frequency")
                    ToolTip.visible: hovered
                 }

                Text {
                    font.pixelSize: 48
                    color: "#aaffffff"
                    text: "Hz"
                    Layout.preferredWidth: 50
                }
            }

            RowLayout {
                Text {
                    font.pixelSize: 24
                    color: "#aaffffff"
                    text: "Clk1:"
                    Layout.preferredWidth: 100
                }

                RadioButton {
                    text: qsTr("Off")
                    checked: qprogrock.clk1 == 0 ? true : false
                    onClicked: qprogrock.clk1 = 0

                    ToolTip.text: qsTr("Turn off clock 1")
                    ToolTip.visible: hovered
                }

                RadioButton {
                    text: qsTr("On")
                    checked: qprogrock.clk1 >= 3500 ? true : false
                    onClicked: qprogrock.clk1 = clk1.value

                    ToolTip.text: qsTr("Turn on clock 1")
                    ToolTip.visible: hovered
                }
            }

            RowLayout {
                id: clk1row
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                SpinBox {
                    id: clk1
                    font.pixelSize: 48
                    value: qprogrock.clk1 < 3500 ? value : qprogrock.clk1
                    from: 3500
                    to: 200000000
                    stepSize: Math.pow(10, step.currentIndex)
                    editable: true
                    onValueModified: qprogrock.clk1 = clk1.value
                    Layout.preferredWidth: 450
                    enabled: qprogrock.clk1 == 0 ? false : true

                    ToolTip.text: qsTr("Set clock 1 frequency")
                    ToolTip.visible: hovered
                 }

                Text {
                    font.pixelSize: 48
                    color: "#aaffffff"
                    text: "Hz"
                    Layout.preferredWidth: 50
                }
            }

            RowLayout {
                Text {
                    font.pixelSize: 24
                    color: "#aaffffff"
                    text: "Clk2:"
                    Layout.preferredWidth: 100
                }

                RadioButton {
                    text: qsTr("Off")
                    checked: qprogrock.clk2 == 0 ? true : false
                    onClicked: qprogrock.clk2 = 0

                    ToolTip.text: qsTr("Turn off clock 2")
                    ToolTip.visible: hovered
                }

                RadioButton {
                    text: qsTr("On")
                    checked: qprogrock.clk2 >= 3500 ? true : false
                    onClicked: qprogrock.clk2 = clk2.value

                    ToolTip.text: qsTr("Turn on clock 2")
                    ToolTip.visible: hovered
                }
            }

            RowLayout {
                id: clk2row
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                SpinBox {
                    id: clk2
                    font.pixelSize: 48
                    value: qprogrock.clk2 < 3500 ? value : qprogrock.clk2
                    from: 3500
                    to: 200000000
                    stepSize: Math.pow(10, step.currentIndex)
                    editable: true
                    onValueModified: qprogrock.clk2 = clk1.value
                    Layout.preferredWidth: 450
                    enabled: qprogrock.clk2 == 0 ? false : true

                    ToolTip.text: qsTr("Set clock 2 frequency")
                    ToolTip.visible: hovered
                 }

                Text {
                    font.pixelSize: 48
                    color: "#aaffffff"
                    text: "Hz"
                    Layout.preferredWidth: 50
                }
            }
        }
    }

    Component {
        id: configView

        ColumnLayout {
            Text {
                font.pixelSize: 24
                color: "#aaffffff"
                text: "GPS correction threshold:"
            }

            SpinBox {
                id: gpsCorrectionThreshold
                font.pixelSize: 24
                value: qprogrock.gpsCorrectionThreshold
                from: 0
                to: 120
                stepSize: 1
                editable: true
                onValueModified: qprogrock.gpsCorrectionThreshold = gpsCorrectionThreshold.value
                Layout.preferredWidth: 150

                ToolTip.text: qsTr("Threshold at which the frequency is adjusted from GPS")
                ToolTip.visible: hovered
            }

            RowLayout {
                Text {
                    font.pixelSize: 24
                    color: "#aaffffff"
                    text: "RefClk:"
                }
            }

            RowLayout {
                SpinBox {
                    id: refClk
                    font.pixelSize: 24
                    value: qprogrock.refClk
                    from: 3500
                    to: 200000000
                    stepSize: 1
                    editable: true
                    onValueModified: qprogrock.refClk = refClk.value
                    Layout.preferredWidth: 300

                    ToolTip.text: qsTr("Set reference clock when GPS not used")
                    ToolTip.visible: hovered
                }

                Text {
                    font.pixelSize: 24
                    color: "#aaffffff"
                    text: "Hz"
                    Layout.preferredWidth: 50
                }
            }

            RowLayout {
                Text {
                    font.pixelSize: 24
                    color: "#aaffffff"
                    text: "Calibration:"
                }

                Text {
                    font.pixelSize: 24
                    color: "#aaffffff"
                    text: qprogrock.calibration.toString(16)
                }
            }

            RowLayout {
                Text {
                    font.pixelSize: 24
                    color: "#aaffffff"
                    text: "EEpromId:"
                }

                Text {
                    font.pixelSize: 24
                    color: "#aaffffff"
                    text: qprogrock.eepromId.toString(16)
                }
            }

            Button {
                text: "Factory Reset"
                onClicked: qprogrock.eepromId = 0
                Layout.preferredWidth: 150
            }
        }
    }

    Component {
        id: helpView

        ColumnLayout {
            Text {
                text: qprogrock.programName + " - " + qprogrock.programVersion + " - " + qprogrock.qtVersion + " - help"
                font.pixelSize: 24
                color: "#aaffffff"
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }

            Text {
                text: "Bank"
                font.bold: true
                color: "#aaffffff"
            }

            Text {
                text: "Select the bank. Must match the config set on the ProgRock."
                color: "#aaffffff"
            }

            Text {
                text: "Step"
                font.bold: true
                color: "#aaffffff"
            }

            Text {
                text: "Select the step size to increment/decrement the frequency using the -/+ buttons."
                color: "#aaffffff"
            }

            Text {
                text: "Clk0/Clk1"
                font.bold: true
                color: "#aaffffff"
            }

            Text {
                text: "Frequency of the first/second oscillator."
                color: "#aaffffff"
            }

            Text {
                text: "On/Off"
                font.bold: true
                color: "#aaffffff"
            }

            Text {
                text: "Turn the first/second oscillator on and off."
                color: "#aaffffff"
            }

            Text {
                text: "Device"
                font.bold: true
                color: "#aaffffff"
            }

            Text {
                text: "Select the serial port on which the RockProg is connected."
                color: "#aaffffff"
            }

            Text {
                text: "Firmware"
                font.bold: true
                color: "#aaffffff"
            }

            Text {
                text: "Show the detected ProgRock firmware version. Not all versions can be detected."
                color: "#aaffffff"
            }

            Text {
                text: "RefClk"
                font.bold: true
                color: "#aaffffff"
            }

            Text {
                text: "Show the current reference clock."
                color: "#aaffffff"
            }

            Text {
                text: "Config"
                font.bold: true
                color: "#aaffffff"
            }

            Text {
                text: "Access to more advanced configs."
                color: "#aaffffff"
            }

            Text {
                text: "?"
                font.bold: true
                color: "#aaffffff"
            }

            Text {
                text: "This help or config help if clicked a second time or from config screen."
                color: "#aaffffff"
            }

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }
    }

    Component {
        id: help2View

        ColumnLayout {
            Text {
                font.pixelSize: 24
                color: "#aaffffff"
                text: qprogrock.programName + " - " + qprogrock.programVersion + " - " + qprogrock.qtVersion + " - config help"
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }

            Text {
                text: "GPS correction threshold"
                font.bold: true
                color: "#aaffffff"
            }

            Text {
                text: "Number of seconds after which the reference clock is adjusted to the GPS if present."
                color: "#aaffffff"
            }

            Text {
                text: "RefClk"
                font.bold: true
                color: "#aaffffff"
            }

            Text {
                text: "Allows to adjust the reference clock if GPS is not present."
                color: "#aaffffff"
            }

            Text {
                text: "Calibration"
                font.bold: true
                color: "#aaffffff"
            }

            Text {
                text: "Read-only of the internal calibration register (28)."
                color: "#aaffffff"
            }

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
        }
    }

    footer: ToolBar {
        id: toolbar

        background: Rectangle {
            color: "#000000"
            border.width: 0
            radius: 4
        }

        RowLayout {
            anchors.fill: parent

            Text {
                text: qsTr("Device: ")
                color: "#aaffffff"
            }

            ComboBox {
                id: devicelist
                model: qprogrock.deviceList
                onActivated: qprogrock.deviceName = currentText
                onPressedChanged: if (pressed) qprogrock.listSerialPorts()
                Layout.preferredWidth: 100

                delegate: ItemDelegate {
                    width: devicelist.width
                    text: devicelist.textRole ? (Array.isArray(devicelist.model) ? modelData[devicelist.textRole] : model[devicelist.textRole]) : modelData
                    font.weight: devicelist.currentIndex === index ? Font.DemiBold : Font.Normal
                    font.family: toolbar.font.family
                    font.pointSize: toolbar.font.pointSize
                    highlighted: devicelist.highlightedIndex === index
                    hoverEnabled: devicelist.hoverEnabled
                }

                contentItem: Text {
                    leftPadding: 0
                    rightPadding: 0

                    text: devicelist.displayText
                    font: devicelist.font
                    color: qprogrock.connected ? "#21be2b" : "#ff0000"
                    verticalAlignment: Text.AlignVCenter
                }

                popup: Popup {
                    y: devicelist.height - 1
                    width: devicelist.width
                    implicitHeight: contentItem.implicitHeight
                    padding: 1

                    contentItem: ListView {
                        clip: false
                        implicitHeight: contentHeight
                        model: devicelist.popup.visible ? devicelist.delegateModel : null
                        currentIndex: devicelist.highlightedIndex

                        ScrollIndicator.vertical: ScrollIndicator { }
                    }

                    background: Rectangle {
                        color: "#000000"
                    }
                }

                background: Rectangle {
                    color: "#00000000"
                    border.width: 1
                    Layout.fillWidth: true
                }

                ToolTip.text: qsTr("Select serial port")
                ToolTip.visible: hovered
            }

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
            }

            Text {
                color: "#aaffffff"
                text: "Firmware: "
            }

            Text {
                color: "#aaffffff"
                text: qprogrock.firmware
                Layout.preferredWidth: 80

                ToolTip.text: qsTr("Detected ProgRock firmware version")
                ToolTip.visible: firmwareversion.containsMouse
                MouseArea {
                    id: firmwareversion
                    anchors.fill: parent
                    hoverEnabled: true
                }
            }

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
            }

            Text {
                color: "#aaffffff"
                text: "RefClk: "
            }

            Text {
                color: "#aaffffff"
                text: qprogrock.refClk
                horizontalAlignment: Text.AlignRight
                Layout.preferredWidth: 80

                ToolTip.text: qsTr("Current reference clock frequency")
                ToolTip.visible: referenceclock.containsMouse
                MouseArea {
                    id: referenceclock
                    anchors.fill: parent
                    hoverEnabled: true
                }
            }

            Text {
                color: "#aaffffff"
                text: "Hz"
            }

            Button {
                text: stack.depth == 1 ? "Config" : "Done"
                onClicked: stack.depth == 1 ? stack.push(configView) : stack.pop()
                Layout.preferredWidth: 80

                ToolTip.text: qsTr("Access advanced configs")
                ToolTip.visible: hovered
            }

            Button {
                text: "?"
                onClicked: stack.depth == 1 ? stack.push(helpView) : stack.depth == 2 ? stack.push(help2View) : stack.pop()
                Layout.preferredWidth: 40

                ToolTip.text: qsTr("Help")
                ToolTip.visible: hovered
            }
        }
    }
}
